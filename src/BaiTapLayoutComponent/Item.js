import React from 'react';

function Item() {
	return (
		<>
			<div className="card bg-light border-0 h-100">
				<div className="card-body text-center p-4 pt-0 pt-lg-0">
					<div className="feature d-inline-block bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4 p-3">
						<i className="fab fa-bootstrap" />
					</div>
					<h2 className="fs-4 fw-bold">Fresh new layout</h2>
					<p className="mb-0">
						With Bootstrap 5, we've created a fresh new layout for this
						template!
					</p>
				</div>
			</div>
		</>
	);
}

export default Item;
