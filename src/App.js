import './App.css';
import Banner from './BaiTapLayoutComponent/Banner';
import Footer from './BaiTapLayoutComponent/Footer';
import Header from './BaiTapLayoutComponent/Header';
import Item from './BaiTapLayoutComponent/Item';

function BaiTapThucHanhLayout() {
	return (
		<div className="BaiTapThucHanhLayout">
			<Header />
			<Banner />

			{/* Item */}
			<div className="pt-4">
				<div className="container px-lg-5">
					<div className="row gx-lg-5">
						<div className="col-lg-4 col-xxl-4 mb-5">
							<Item />
						</div>
						<div className="col-lg-4 col-xxl-4 mb-5">
							<Item />
						</div>
						<div className="col-lg-4 col-xxl-4 mb-5">
							<Item />
						</div>
						<div className="col-lg-4 col-xxl-4 mb-5">
							<Item />
						</div>
						<div className="col-lg-4 col-xxl-4 mb-5">
							<Item />
						</div>
						<div className="col-lg-4 col-xxl-4 mb-5">
							<Item />
						</div>
					</div>
				</div>
			</div>
			<Footer />
		</div>
	);
}

export default BaiTapThucHanhLayout;
